# K6-quickBenchmark

Files used to quickly benchmark before and after performance on an endpoint. 

The code outside the default function will run once per virtual user (vu). 

The code inside the default function will be run by each vu for the entire `duration`, through the number of `iteration`(s), or only once if no options are used. 

## Installation 
```sh
$ brew install k6
```

## Usage
```sh
$ k6 run script.js
```


## Links

[Official Documentation](https://k6.io/docs/) <br>
[k6 github](https://github.com/k6io/k6)





