import http from 'k6/http';
import { check, sleep } from 'k6';

//init code (run once per vu)

export let options = {
    vus: 10,
    // iterations: 30,
    duration: '3s'
}

export default function () {
  // virtual user (vu) code and run over and over until test is over
  const url = 'http://localhost:3500';
 
 
  const params = {
      headers: {
        'x-access-token': ''
      }
  }

  const res = http.get(url, params);
  // console.log('Status: ', res.status);
  // console.log(res.status_text);

  check(res, { 'status was 200': (r) => r.status == 200 });
  sleep(1);
}




